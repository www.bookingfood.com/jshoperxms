define([ 
         'angular',
         'angular-route',
         'angular-resource',
         'jquery',
         'bootstrap3',
         'ngfileupload',
         './interceptors',
         './services',
         './login/index',
         './index/index',
         './goodstype/index',
         './goodsattribute/index',
         './goodstypebrand/index',
         './feedback/index',
         './posts/index',
         './member/index',
         './brand/index',
         './membergroup/index',
         './membergrade/index'], function(angular) {
	'use strict';
	var app=angular.module('app', [ 'loginmodule', 'indexmodule',
			'goodstypemodule','goodsattributemodule','goodstypebrandmodule','feedbackmodule', 'postsmodule','membermodule', 'membergroupmodule', 'membergrademodule', 'ngRoute','ngResource','myservices','myinterceptors','brandmodule']);
	return app;
});
