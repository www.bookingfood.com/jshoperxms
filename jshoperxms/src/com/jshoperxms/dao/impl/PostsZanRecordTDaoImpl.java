package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.PostsZanRecordTDao;
import com.jshoperxms.entity.PostsZanRecordT;

@Repository("postsZanRecordTDao")
public class PostsZanRecordTDaoImpl extends BaseTDaoImpl<PostsZanRecordT> implements PostsZanRecordTDao {

	private static final Logger log = LoggerFactory.getLogger(PostsZanRecordTDaoImpl.class);
	
}
