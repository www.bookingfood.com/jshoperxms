package com.jshoperxms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the feedback_t database table.
 * 
 */
@Entity
@Table(name = "feedback_t")
@NamedQuery(name = "FeedbackT.findAll", query = "SELECT f FROM FeedbackT f")
public class FeedbackT implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String content;

	private String status;

	private String devicetype;

	private String basicinfoid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;

	private int versiont;

	private String dataid;

	private String feedbacksource;
	
	private String feedbackpurpose;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBasicinfoid() {
		return basicinfoid;
	}

	public void setBasicinfoid(String basicinfoid) {
		this.basicinfoid = basicinfoid;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public int getVersiont() {
		return versiont;
	}

	public void setVersiont(int versiont) {
		this.versiont = versiont;
	}

	public String getDataid() {
		return dataid;
	}

	public void setDataid(String dataid) {
		this.dataid = dataid;
	}

	public String getFeedbacksource() {
		return feedbacksource;
	}

	public void setFeedbacksource(String feedbacksource) {
		this.feedbacksource = feedbacksource;
	}

	public String getFeedbackpurpose() {
		return feedbackpurpose;
	}

	public void setFeedbackpurpose(String feedbackpurpose) {
		this.feedbackpurpose = feedbackpurpose;
	}

	public String getDevicetype() {
		return devicetype;
	}

	public void setDevicetype(String devicetype) {
		this.devicetype = devicetype;
	}

}
