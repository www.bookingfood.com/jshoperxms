package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.SiteNavigationT;
import com.jshoperxms.service.SiteNavigationTService;

@Service("siteNavigationTService")
@Scope("prototype")
public class SiteNavigationTServiceImpl extends BaseTServiceImpl<SiteNavigationT> implements SiteNavigationTService {

}
