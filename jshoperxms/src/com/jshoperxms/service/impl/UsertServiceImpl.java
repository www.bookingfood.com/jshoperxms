package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.UserT;
import com.jshoperxms.service.UsertService;

@Service("usertService")
@Scope("prototype")
public class UsertServiceImpl extends BaseTServiceImpl<UserT>implements UsertService {


}
